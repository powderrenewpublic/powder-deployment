# Campus

The campus-wide ("Rooftop" and "Fixed endpoint" types) area is bound
by the Innovation Zone defined in `DA-19-923A1.pdf` (see page 3).  The
corners of the relevant polygon are:

| Lat        | Lon         |
| ---------- | ----------- |
| 40.77309 N | 111.82639 W |
| 40.77313 N | 111.85669 W |
| 40.75076 N | 111.82661 W |
| 40.75096 N | 111.85656 W |

# Dense

The dense deployment area (containing "Dense" node types) is more a label
of convenience than a strict designation, but for our purposes we are
interested in the area around:

| Lat        | Lon         |
| ---------- | ----------- |
| 40.77313 N | 111.83440 W |
| 40.77313 N | 111.84400 W |
| 40.76500 N | 111.83440 W |
| 40.76500 N | 111.84400 W |
