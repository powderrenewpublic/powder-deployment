
		--==[ SPLAT! HD v1.4.0 Path Analysis ]==--

---------------------------------------------------------------------------

Transmitter site: Medical Tower
Site location: 40.7674 North / 111.8312 West (40� 46' 2" N / 111� 49' 52" W)
Ground elevation: 4967.19 feet AMSL
Antenna height: 120.00 feet AGL / 5087.19 feet AMSL
Distance to Bookstore: 0.89 miles
Azimuth to Bookstore: 255.30 degrees
Depression angle to Bookstore: -4.4643 degrees

---------------------------------------------------------------------------

Receiver site: Bookstore
Site location: 40.7641 North / 111.8476 West (40� 45' 50" N / 111� 50' 51" W)
Ground elevation: 4717.85 feet AMSL
Antenna height: 5.00 feet AGL / 4722.85 feet AMSL
Distance to Medical Tower: 0.89 miles
Azimuth to Medical Tower: 75.29 degrees
Elevation angle to Medical Tower: +4.4515 degrees

---------------------------------------------------------------------------

ITWOM Version 3.0 Parameters Used In This Analysis:

Earth's Dielectric Constant: 5.000
Earth's Conductivity: 0.001 Siemens/meter
Atmospheric Bending Constant (N-units): 301.000 ppm
Frequency: 2500.000 MHz
Radio Climate: 5 (Continental Temperate)
Polarization: 0 (Horizontal)
Fraction of Situations: 50.0%
Fraction of Time: 50.0%
Transmitter ERP: 10.0 milliwatts (+10.00 dBm)
Transmitter EIRP: 16.4 milliwatts (+12.14 dBm)

---------------------------------------------------------------------------

Summary For The Link Between Medical Tower and Bookstore:

Free space path loss: 103.52 dB
ITWOM Version 3.0 path loss: 104.95 dB
Attenuation due to terrain shielding: 1.43 dB
Field strength at Bookstore: 52.41 dBuV/meter
Signal power level at Bookstore: -92.81 dBm
Signal power density at Bookstore: -93.38 dBW per square meter
Voltage across a 50 ohm dipole at Bookstore: 6.55 uV (16.32 dBuV)
Voltage across a 75 ohm dipole at Bookstore: 8.02 uV (18.08 dBuV)
Mode of propagation: Line of Sight
ITWOM error number: 0 (No error)

---------------------------------------------------------------------------


No obstructions to LOS path due to terrain were detected by SPLAT! HD

Antenna at Bookstore must be raised to at least 13.00 feet AGL
to clear the first Fresnel zone.

Antenna at Bookstore must be raised to at least 7.00 feet AGL
to clear 60% of the first Fresnel zone.
