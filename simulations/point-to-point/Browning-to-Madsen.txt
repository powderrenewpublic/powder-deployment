
		--==[ SPLAT! HD v1.4.0 Path Analysis ]==--

---------------------------------------------------------------------------

Transmitter site: Browning
Site location: 40.7663 North / 111.8477 West (40� 45' 58" N / 111� 50' 51" W)
Ground elevation: 4727.69 feet AMSL
Antenna height: 80.00 feet AGL / 4807.69 feet AMSL
Distance to Madsen: 0.83 miles
Azimuth to Madsen: 134.19 degrees
Depression angle to Madsen: -0.0428 degrees
Depression angle to the first obstruction: -0.0229 degrees

---------------------------------------------------------------------------

Receiver site: Madsen
Site location: 40.7579 North / 111.8363 West (40� 45' 28" N / 111� 50' 10" W)
Ground elevation: 4799.87 feet AMSL
Antenna height: 5.00 feet AGL / 4804.87 feet AMSL
Distance to Browning: 0.83 miles
Azimuth to Browning: 314.20 degrees
Elevation angle to Browning: +0.0308 degrees
Elevation angle to the first obstruction: +0.5187 degrees

---------------------------------------------------------------------------

ITWOM Version 3.0 Parameters Used In This Analysis:

Earth's Dielectric Constant: 5.000
Earth's Conductivity: 0.001 Siemens/meter
Atmospheric Bending Constant (N-units): 301.000 ppm
Frequency: 2500.000 MHz
Radio Climate: 5 (Continental Temperate)
Polarization: 0 (Horizontal)
Fraction of Situations: 50.0%
Fraction of Time: 50.0%
Transmitter ERP: 10.0 milliwatts (+10.00 dBm)
Transmitter EIRP: 16.4 milliwatts (+12.14 dBm)

---------------------------------------------------------------------------

Summary For The Link Between Browning and Madsen:

Free space path loss: 102.96 dB
ITWOM Version 3.0 path loss: 109.81 dB
Attenuation due to terrain shielding: 6.85 dB
Field strength at Madsen: 47.55 dBuV/meter
Signal power level at Madsen: -97.67 dBm
Signal power density at Madsen: -98.24 dBW per square meter
Voltage across a 50 ohm dipole at Madsen: 3.74 uV (11.46 dBuV)
Voltage across a 75 ohm dipole at Madsen: 4.58 uV (13.22 dBuV)
Mode of propagation: Line of Sight
ITWOM error number: 0 (No error)

---------------------------------------------------------------------------

Between Madsen and Browning, SPLAT! HD detected obstructions at:

    40.7582 N, 111.8368 W,  0.03 miles, 4806.43 feet AMSL

Antenna at Madsen must be raised to at least 7.00 feet AGL
to clear all obstructions detected by SPLAT! HD.

Antenna at Madsen must be raised to at least 15.00 feet AGL
to clear the first Fresnel zone.

Antenna at Madsen must be raised to at least 12.00 feet AGL
to clear 60% of the first Fresnel zone.
