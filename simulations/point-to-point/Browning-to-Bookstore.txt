
		--==[ SPLAT! HD v1.4.0 Path Analysis ]==--

---------------------------------------------------------------------------

Transmitter site: Browning
Site location: 40.7663 North / 111.8477 West (40� 45' 58" N / 111� 50' 51" W)
Ground elevation: 4727.69 feet AMSL
Antenna height: 80.00 feet AGL / 4807.69 feet AMSL
Distance to Bookstore: 0.15 miles
Azimuth to Bookstore: 177.02 degrees
Depression angle to Bookstore: -6.2511 degrees

---------------------------------------------------------------------------

Receiver site: Bookstore
Site location: 40.7641 North / 111.8476 West (40� 45' 50" N / 111� 50' 51" W)
Ground elevation: 4717.85 feet AMSL
Antenna height: 5.00 feet AGL / 4722.85 feet AMSL
Distance to Browning: 0.15 miles
Azimuth to Browning: 357.02 degrees
Elevation angle to Browning: +6.2490 degrees

---------------------------------------------------------------------------

ITWOM Version 3.0 Parameters Used In This Analysis:

Earth's Dielectric Constant: 5.000
Earth's Conductivity: 0.001 Siemens/meter
Atmospheric Bending Constant (N-units): 301.000 ppm
Frequency: 2500.000 MHz
Radio Climate: 5 (Continental Temperate)
Polarization: 0 (Horizontal)
Fraction of Situations: 50.0%
Fraction of Time: 50.0%
Transmitter ERP: 10.0 milliwatts (+10.00 dBm)
Transmitter EIRP: 16.4 milliwatts (+12.14 dBm)

---------------------------------------------------------------------------

Summary For The Link Between Browning and Bookstore:

Free space path loss: 87.94 dB
ITWOM Version 3.0 path loss: 98.28 dB
Attenuation due to terrain shielding: 10.34 dB
Field strength at Bookstore: 59.08 dBuV/meter
Signal power level at Bookstore: -86.14 dBm
Signal power density at Bookstore: -86.71 dBW per square meter
Voltage across a 50 ohm dipole at Bookstore: 14.11 uV (22.99 dBuV)
Voltage across a 75 ohm dipole at Bookstore: 17.28 uV (24.75 dBuV)
Mode of propagation: Line of Sight
ITWOM error number: 4
  Warning: Some parameters are out of range.
  Results are probably invalid.

---------------------------------------------------------------------------


No obstructions to LOS path due to terrain were detected by SPLAT! HD

The first Fresnel zone is clear.

60% of the first Fresnel zone is clear.
