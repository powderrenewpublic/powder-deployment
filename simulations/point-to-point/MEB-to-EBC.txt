
		--==[ SPLAT! HD v1.4.0 Path Analysis ]==--

---------------------------------------------------------------------------

Transmitter site: MEB
Site location: 40.7688 North / 111.8458 West (40� 46' 7" N / 111� 50' 44" W)
Ground elevation: 4793.31 feet AMSL
Antenna height: 50.00 feet AGL / 4843.31 feet AMSL
Distance to EBC: 0.41 miles
Azimuth to EBC: 100.57 degrees
Elevation angle to EBC: +1.1528 degrees

---------------------------------------------------------------------------

Receiver site: EBC
Site location: 40.7677 North / 111.8381 West (40� 46' 3" N / 111� 50' 17" W)
Ground elevation: 4881.89 feet AMSL
Antenna height: 5.00 feet AGL / 4886.89 feet AMSL
Distance to MEB: 0.41 miles
Azimuth to MEB: 280.58 degrees
Depression angle to MEB: -1.1587 degrees

---------------------------------------------------------------------------

ITWOM Version 3.0 Parameters Used In This Analysis:

Earth's Dielectric Constant: 5.000
Earth's Conductivity: 0.001 Siemens/meter
Atmospheric Bending Constant (N-units): 301.000 ppm
Frequency: 2500.000 MHz
Radio Climate: 5 (Continental Temperate)
Polarization: 0 (Horizontal)
Fraction of Situations: 50.0%
Fraction of Time: 50.0%
Transmitter ERP: 10.0 milliwatts (+10.00 dBm)
Transmitter EIRP: 16.4 milliwatts (+12.14 dBm)

---------------------------------------------------------------------------

Summary For The Link Between MEB and EBC:

Free space path loss: 96.80 dB
ITWOM Version 3.0 path loss: 118.15 dB
Attenuation due to terrain shielding: 21.36 dB
Field strength at EBC: 39.21 dBuV/meter
Signal power level at EBC: -106.01 dBm
Signal power density at EBC: -106.58 dBW per square meter
Voltage across a 50 ohm dipole at EBC: 1.43 uV (3.12 dBuV)
Voltage across a 75 ohm dipole at EBC: 1.75 uV (4.88 dBuV)
Mode of propagation: Line of Sight
ITWOM error number: 4
  Warning: Some parameters are out of range.
  Results are probably invalid.

---------------------------------------------------------------------------


No obstructions to LOS path due to terrain were detected by SPLAT! HD

The first Fresnel zone is clear.

60% of the first Fresnel zone is clear.
