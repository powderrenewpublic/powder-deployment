
		--==[ SPLAT! HD v1.4.0 Path Analysis ]==--

---------------------------------------------------------------------------

Transmitter site: Medical Tower
Site location: 40.7674 North / 111.8312 West (40� 46' 2" N / 111� 49' 52" W)
Ground elevation: 4967.19 feet AMSL
Antenna height: 120.00 feet AGL / 5087.19 feet AMSL
Distance to Madsen: 0.71 miles
Azimuth to Madsen: 202.31 degrees
Depression angle to Madsen: -4.3174 degrees

---------------------------------------------------------------------------

Receiver site: Madsen
Site location: 40.7579 North / 111.8363 West (40� 45' 28" N / 111� 50' 10" W)
Ground elevation: 4799.87 feet AMSL
Antenna height: 5.00 feet AGL / 4804.87 feet AMSL
Distance to Medical Tower: 0.71 miles
Azimuth to Medical Tower: 22.31 degrees
Elevation angle to Medical Tower: +4.3072 degrees

---------------------------------------------------------------------------

ITWOM Version 3.0 Parameters Used In This Analysis:

Earth's Dielectric Constant: 5.000
Earth's Conductivity: 0.001 Siemens/meter
Atmospheric Bending Constant (N-units): 301.000 ppm
Frequency: 2500.000 MHz
Radio Climate: 5 (Continental Temperate)
Polarization: 0 (Horizontal)
Fraction of Situations: 50.0%
Fraction of Time: 50.0%
Transmitter ERP: 10.0 milliwatts (+10.00 dBm)
Transmitter EIRP: 16.4 milliwatts (+12.14 dBm)

---------------------------------------------------------------------------

Summary For The Link Between Medical Tower and Madsen:

Free space path loss: 101.60 dB
ITWOM Version 3.0 path loss: 103.55 dB
Attenuation due to terrain shielding: 1.95 dB
Field strength at Madsen: 53.81 dBuV/meter
Signal power level at Madsen: -91.41 dBm
Signal power density at Madsen: -91.98 dBW per square meter
Voltage across a 50 ohm dipole at Madsen: 7.69 uV (17.72 dBuV)
Voltage across a 75 ohm dipole at Madsen: 9.42 uV (19.48 dBuV)
Mode of propagation: Line of Sight
ITWOM error number: 0 (No error)

---------------------------------------------------------------------------


No obstructions to LOS path due to terrain were detected by SPLAT! HD

Antenna at Madsen must be raised to at least 9.00 feet AGL
to clear the first Fresnel zone.

Antenna at Madsen must be raised to at least 7.00 feet AGL
to clear 60% of the first Fresnel zone.
