
		--==[ SPLAT! HD v1.4.0 Path Analysis ]==--

---------------------------------------------------------------------------

Transmitter site: Dentistry
Site location: 40.7572 North / 111.8315 West (40� 45' 25" N / 111� 49' 53" W)
Ground elevation: 4839.24 feet AMSL
Antenna height: 50.00 feet AGL / 4889.24 feet AMSL
Distance to EBC: 0.81 miles
Azimuth to EBC: 334.50 degrees
Depression angle to EBC: -0.0374 degrees

---------------------------------------------------------------------------

Receiver site: EBC
Site location: 40.7677 North / 111.8381 West (40� 46' 3" N / 111� 50' 17" W)
Ground elevation: 4881.89 feet AMSL
Antenna height: 5.00 feet AGL / 4886.89 feet AMSL
Distance to Dentistry: 0.81 miles
Azimuth to Dentistry: 154.49 degrees
Elevation angle to Dentistry: +0.0257 degrees

---------------------------------------------------------------------------

ITWOM Version 3.0 Parameters Used In This Analysis:

Earth's Dielectric Constant: 5.000
Earth's Conductivity: 0.001 Siemens/meter
Atmospheric Bending Constant (N-units): 301.000 ppm
Frequency: 2500.000 MHz
Radio Climate: 5 (Continental Temperate)
Polarization: 0 (Horizontal)
Fraction of Situations: 50.0%
Fraction of Time: 50.0%
Transmitter ERP: 10.0 milliwatts (+10.00 dBm)
Transmitter EIRP: 16.4 milliwatts (+12.14 dBm)

---------------------------------------------------------------------------

Summary For The Link Between Dentistry and EBC:

Free space path loss: 102.70 dB
ITWOM Version 3.0 path loss: 102.31 dB
Attenuation due to terrain shielding: -0.39 dB
Field strength at EBC: 55.04 dBuV/meter
Signal power level at EBC: -90.17 dBm
Signal power density at EBC: -90.74 dBW per square meter
Voltage across a 50 ohm dipole at EBC: 8.87 uV (18.96 dBuV)
Voltage across a 75 ohm dipole at EBC: 10.86 uV (20.72 dBuV)
Mode of propagation: Single Horizon Diffraction Dominant
ITWOM error number: 0 (No error)

---------------------------------------------------------------------------


No obstructions to LOS path due to terrain were detected by SPLAT! HD

Antenna at EBC must be raised to at least 29.00 feet AGL
to clear the first Fresnel zone.

Antenna at EBC must be raised to at least 18.00 feet AGL
to clear 60% of the first Fresnel zone.
