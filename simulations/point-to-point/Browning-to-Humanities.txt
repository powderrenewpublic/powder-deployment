
		--==[ SPLAT! HD v1.4.0 Path Analysis ]==--

---------------------------------------------------------------------------

Transmitter site: Browning
Site location: 40.7663 North / 111.8477 West (40� 45' 58" N / 111� 50' 51" W)
Ground elevation: 4727.69 feet AMSL
Antenna height: 80.00 feet AGL / 4807.69 feet AMSL
Distance to Humanities: 0.26 miles
Azimuth to Humanities: 112.31 degrees
Depression angle to Humanities: -0.8130 degrees

---------------------------------------------------------------------------

Receiver site: Humanities
Site location: 40.7649 North / 111.8432 West (40� 45' 53" N / 111� 50' 35" W)
Ground elevation: 4783.46 feet AMSL
Antenna height: 5.00 feet AGL / 4788.46 feet AMSL
Distance to Browning: 0.26 miles
Azimuth to Browning: 292.31 degrees
Elevation angle to Browning: +0.8093 degrees

---------------------------------------------------------------------------

ITWOM Version 3.0 Parameters Used In This Analysis:

Earth's Dielectric Constant: 5.000
Earth's Conductivity: 0.001 Siemens/meter
Atmospheric Bending Constant (N-units): 301.000 ppm
Frequency: 2500.000 MHz
Radio Climate: 5 (Continental Temperate)
Polarization: 0 (Horizontal)
Fraction of Situations: 50.0%
Fraction of Time: 50.0%
Transmitter ERP: 10.0 milliwatts (+10.00 dBm)
Transmitter EIRP: 16.4 milliwatts (+12.14 dBm)

---------------------------------------------------------------------------

Summary For The Link Between Browning and Humanities:

Free space path loss: 92.76 dB
ITWOM Version 3.0 path loss: 114.15 dB
Attenuation due to terrain shielding: 21.39 dB
Field strength at Humanities: 43.21 dBuV/meter
Signal power level at Humanities: -102.01 dBm
Signal power density at Humanities: -102.58 dBW per square meter
Voltage across a 50 ohm dipole at Humanities: 2.27 uV (7.12 dBuV)
Voltage across a 75 ohm dipole at Humanities: 2.78 uV (8.88 dBuV)
Mode of propagation: Line of Sight
ITWOM error number: 4
  Warning: Some parameters are out of range.
  Results are probably invalid.

---------------------------------------------------------------------------


No obstructions to LOS path due to terrain were detected by SPLAT! HD

The first Fresnel zone is clear.

60% of the first Fresnel zone is clear.
