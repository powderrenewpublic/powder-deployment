
		--==[ SPLAT! HD v1.4.0 Path Analysis ]==--

---------------------------------------------------------------------------

Transmitter site: Honors
Site location: 40.7644 North / 111.8370 West (40� 45' 51" N / 111� 50' 13" W)
Ground elevation: 4845.80 feet AMSL
Antenna height: 100.00 feet AGL / 4945.80 feet AMSL
Distance to Law73: 0.80 miles
Azimuth to Law73: 256.19 degrees
Depression angle to Law73: -4.1313 degrees
Depression angle to the first obstruction: -4.0301 degrees

---------------------------------------------------------------------------

Receiver site: Law73
Site location: 40.7616 North / 111.8518 West (40� 45' 41" N / 111� 51' 6" W)
Ground elevation: 4635.83 feet AMSL
Antenna height: 5.00 feet AGL / 4640.83 feet AMSL
Distance to Honors: 0.80 miles
Azimuth to Honors: 76.18 degrees
Elevation angle to Honors: +4.1197 degrees
Elevation angle to the first obstruction: +4.3147 degrees

---------------------------------------------------------------------------

ITWOM Version 3.0 Parameters Used In This Analysis:

Earth's Dielectric Constant: 5.000
Earth's Conductivity: 0.001 Siemens/meter
Atmospheric Bending Constant (N-units): 301.000 ppm
Frequency: 2500.000 MHz
Radio Climate: 5 (Continental Temperate)
Polarization: 0 (Horizontal)
Fraction of Situations: 50.0%
Fraction of Time: 50.0%
Transmitter ERP: 10.0 milliwatts (+10.00 dBm)
Transmitter EIRP: 16.4 milliwatts (+12.14 dBm)

---------------------------------------------------------------------------

Summary For The Link Between Honors and Law73:

Free space path loss: 102.65 dB
ITWOM Version 3.0 path loss: 126.01 dB
Attenuation due to terrain shielding: 23.36 dB
Field strength at Law73: 31.35 dBuV/meter
Signal power level at Law73: -113.87 dBm
Signal power density at Law73: -114.44 dBW per square meter
Voltage across a 50 ohm dipole at Law73: 0.58 uV (-4.74 dBuV)
Voltage across a 75 ohm dipole at Law73: 0.71 uV (-2.98 dBuV)
Mode of propagation: Single Horizon Diffraction Dominant
ITWOM error number: 0 (No error)

---------------------------------------------------------------------------

Between Law73 and Honors, SPLAT! HD detected obstructions at:

    40.7620 N, 111.8501 W,  0.10 miles, 4681.76 feet AMSL
    40.7620 N, 111.8498 W,  0.11 miles, 4691.60 feet AMSL

Antenna at Law73 must be raised to at least 15.00 feet AGL
to clear all obstructions detected by SPLAT! HD.

Antenna at Law73 must be raised to at least 32.00 feet AGL
to clear the first Fresnel zone.

Antenna at Law73 must be raised to at least 25.00 feet AGL
to clear 60% of the first Fresnel zone.
