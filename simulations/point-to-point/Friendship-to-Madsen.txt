
		--==[ SPLAT! HD v1.4.0 Path Analysis ]==--

---------------------------------------------------------------------------

Transmitter site: Friendship
Site location: 40.7581 North / 111.8533 West (40� 45' 29" N / 111� 51' 11" W)
Ground elevation: 4668.64 feet AMSL
Antenna height: 120.00 feet AGL / 4788.64 feet AMSL
Distance to Madsen: 0.89 miles
Azimuth to Madsen: 90.86 degrees
Elevation angle to Madsen: +0.1926 degrees

---------------------------------------------------------------------------

Receiver site: Madsen
Site location: 40.7579 North / 111.8363 West (40� 45' 28" N / 111� 50' 10" W)
Ground elevation: 4799.87 feet AMSL
Antenna height: 5.00 feet AGL / 4804.87 feet AMSL
Distance to Friendship: 0.89 miles
Azimuth to Friendship: 270.88 degrees
Depression angle to Friendship: -0.2054 degrees

---------------------------------------------------------------------------

ITWOM Version 3.0 Parameters Used In This Analysis:

Earth's Dielectric Constant: 5.000
Earth's Conductivity: 0.001 Siemens/meter
Atmospheric Bending Constant (N-units): 301.000 ppm
Frequency: 2500.000 MHz
Radio Climate: 5 (Continental Temperate)
Polarization: 0 (Horizontal)
Fraction of Situations: 50.0%
Fraction of Time: 50.0%
Transmitter ERP: 10.0 milliwatts (+10.00 dBm)
Transmitter EIRP: 16.4 milliwatts (+12.14 dBm)

---------------------------------------------------------------------------

Summary For The Link Between Friendship and Madsen:

Free space path loss: 103.50 dB
ITWOM Version 3.0 path loss: 110.82 dB
Attenuation due to terrain shielding: 7.32 dB
Field strength at Madsen: 46.53 dBuV/meter
Signal power level at Madsen: -98.68 dBm
Signal power density at Madsen: -99.25 dBW per square meter
Voltage across a 50 ohm dipole at Madsen: 3.33 uV (10.45 dBuV)
Voltage across a 75 ohm dipole at Madsen: 4.08 uV (12.21 dBuV)
Mode of propagation: Line of Sight
ITWOM error number: 0 (No error)

---------------------------------------------------------------------------


No obstructions to LOS path due to terrain were detected by SPLAT! HD

The first Fresnel zone is clear.

60% of the first Fresnel zone is clear.
