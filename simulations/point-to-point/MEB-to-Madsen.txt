
		--==[ SPLAT! HD v1.4.0 Path Analysis ]==--

---------------------------------------------------------------------------

Transmitter site: MEB
Site location: 40.7688 North / 111.8458 West (40� 46' 7" N / 111� 50' 44" W)
Ground elevation: 4793.31 feet AMSL
Antenna height: 50.00 feet AGL / 4843.31 feet AMSL
Distance to Madsen: 0.90 miles
Azimuth to Madsen: 146.62 degrees
Depression angle to Madsen: -0.4687 degrees
Depression angle to the first obstruction: -0.4678 degrees

---------------------------------------------------------------------------

Receiver site: Madsen
Site location: 40.7579 North / 111.8363 West (40� 45' 28" N / 111� 50' 10" W)
Ground elevation: 4799.87 feet AMSL
Antenna height: 5.00 feet AGL / 4804.87 feet AMSL
Distance to MEB: 0.90 miles
Azimuth to MEB: 326.63 degrees
Elevation angle to MEB: +0.4556 degrees
Elevation angle to the first obstruction: +0.4883 degrees

---------------------------------------------------------------------------

ITWOM Version 3.0 Parameters Used In This Analysis:

Earth's Dielectric Constant: 5.000
Earth's Conductivity: 0.001 Siemens/meter
Atmospheric Bending Constant (N-units): 301.000 ppm
Frequency: 2500.000 MHz
Radio Climate: 5 (Continental Temperate)
Polarization: 0 (Horizontal)
Fraction of Situations: 50.0%
Fraction of Time: 50.0%
Transmitter ERP: 10.0 milliwatts (+10.00 dBm)
Transmitter EIRP: 16.4 milliwatts (+12.14 dBm)

---------------------------------------------------------------------------

Summary For The Link Between MEB and Madsen:

Free space path loss: 103.67 dB
ITWOM Version 3.0 path loss: 125.48 dB
Attenuation due to terrain shielding: 21.81 dB
Field strength at Madsen: 31.88 dBuV/meter
Signal power level at Madsen: -113.34 dBm
Signal power density at Madsen: -113.90 dBW per square meter
Voltage across a 50 ohm dipole at Madsen: 0.62 uV (-4.21 dBuV)
Voltage across a 75 ohm dipole at Madsen: 0.75 uV (-2.44 dBuV)
Mode of propagation: Line of Sight
ITWOM error number: 0 (No error)

---------------------------------------------------------------------------

Between Madsen and MEB, SPLAT! HD detected obstructions at:

    40.7583 N, 111.8367 W,  0.04 miles, 4806.43 feet AMSL

Antenna at Madsen must be raised to at least 6.00 feet AGL
to clear all obstructions detected by SPLAT! HD.

Antenna at Madsen must be raised to at least 20.00 feet AGL
to clear the first Fresnel zone.

Antenna at Madsen must be raised to at least 11.00 feet AGL
to clear 60% of the first Fresnel zone.
