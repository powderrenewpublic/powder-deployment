
		--==[ SPLAT! HD v1.4.0 Path Analysis ]==--

---------------------------------------------------------------------------

Transmitter site: Dentistry
Site location: 40.7572 North / 111.8315 West (40� 45' 25" N / 111� 49' 53" W)
Ground elevation: 4839.24 feet AMSL
Antenna height: 50.00 feet AGL / 4889.24 feet AMSL
Distance to Law73: 1.11 miles
Azimuth to Law73: 286.20 degrees
Depression angle to Law73: -2.4388 degrees
Depression angle to the first obstruction: -2.3706 degrees

---------------------------------------------------------------------------

Receiver site: Law73
Site location: 40.7616 North / 111.8518 West (40� 45' 41" N / 111� 51' 6" W)
Ground elevation: 4635.83 feet AMSL
Antenna height: 5.00 feet AGL / 4640.83 feet AMSL
Distance to Dentistry: 1.11 miles
Azimuth to Dentistry: 106.19 degrees
Elevation angle to Dentistry: +2.4227 degrees
Elevation angle to the first obstruction: +2.6950 degrees

---------------------------------------------------------------------------

ITWOM Version 3.0 Parameters Used In This Analysis:

Earth's Dielectric Constant: 5.000
Earth's Conductivity: 0.001 Siemens/meter
Atmospheric Bending Constant (N-units): 301.000 ppm
Frequency: 2500.000 MHz
Radio Climate: 5 (Continental Temperate)
Polarization: 0 (Horizontal)
Fraction of Situations: 50.0%
Fraction of Time: 50.0%
Transmitter ERP: 10.0 milliwatts (+10.00 dBm)
Transmitter EIRP: 16.4 milliwatts (+12.14 dBm)

---------------------------------------------------------------------------

Summary For The Link Between Dentistry and Law73:

Free space path loss: 105.46 dB
ITWOM Version 3.0 path loss: 191.27 dB
Attenuation due to terrain shielding: 85.81 dB
Field strength at Law73: -33.92 dBuV/meter
Signal power level at Law73: -179.13 dBm
Signal power density at Law73: -179.70 dBW per square meter
Voltage across a 50 ohm dipole at Law73: 0.00 uV (-70.00 dBuV)
Voltage across a 75 ohm dipole at Law73: 0.00 uV (-68.24 dBuV)
Mode of propagation: Double Horizon Diffraction Dominant
ITWOM error number: 0 (No error)

---------------------------------------------------------------------------

Between Law73 and Dentistry, SPLAT! HD detected obstructions at:

    40.7614 N, 111.8508 W,  0.06 miles, 4655.51 feet AMSL
    40.7613 N, 111.8505 W,  0.07 miles, 4662.07 feet AMSL
    40.7613 N, 111.8502 W,  0.09 miles, 4671.92 feet AMSL
    40.7611 N, 111.8494 W,  0.13 miles, 4688.32 feet AMSL

Antenna at Law73 must be raised to at least 26.00 feet AGL
to clear all obstructions detected by SPLAT! HD.

Antenna at Law73 must be raised to at least 54.00 feet AGL
to clear the first Fresnel zone.

Antenna at Law73 must be raised to at least 37.00 feet AGL
to clear 60% of the first Fresnel zone.
