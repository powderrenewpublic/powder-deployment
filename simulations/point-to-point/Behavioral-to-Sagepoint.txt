
		--==[ SPLAT! HD v1.4.0 Path Analysis ]==--

---------------------------------------------------------------------------

Transmitter site: Behavioral
Site location: 40.7613 North / 111.8463 West (40� 45' 40" N / 111� 50' 46" W)
Ground elevation: 4717.85 feet AMSL
Antenna height: 120.00 feet AGL / 4837.85 feet AMSL
Distance to Sagepoint: 0.83 miles
Azimuth to Sagepoint: 82.98 degrees
Elevation angle to Sagepoint: +1.0258 degrees

---------------------------------------------------------------------------

Receiver site: Sagepoint
Site location: 40.7628 North / 111.8306 West (40� 45' 46" N / 111� 49' 50" W)
Ground elevation: 4911.42 feet AMSL
Antenna height: 5.00 feet AGL / 4916.42 feet AMSL
Distance to Behavioral: 0.83 miles
Azimuth to Behavioral: 262.99 degrees
Depression angle to Behavioral: -1.0378 degrees

---------------------------------------------------------------------------

ITWOM Version 3.0 Parameters Used In This Analysis:

Earth's Dielectric Constant: 5.000
Earth's Conductivity: 0.001 Siemens/meter
Atmospheric Bending Constant (N-units): 301.000 ppm
Frequency: 2500.000 MHz
Radio Climate: 5 (Continental Temperate)
Polarization: 0 (Horizontal)
Fraction of Situations: 50.0%
Fraction of Time: 50.0%
Transmitter ERP: 10.0 milliwatts (+10.00 dBm)
Transmitter EIRP: 16.4 milliwatts (+12.14 dBm)

---------------------------------------------------------------------------

Summary For The Link Between Behavioral and Sagepoint:

Free space path loss: 102.90 dB
ITWOM Version 3.0 path loss: 109.85 dB
Attenuation due to terrain shielding: 6.95 dB
Field strength at Sagepoint: 47.51 dBuV/meter
Signal power level at Sagepoint: -97.71 dBm
Signal power density at Sagepoint: -98.28 dBW per square meter
Voltage across a 50 ohm dipole at Sagepoint: 3.72 uV (11.42 dBuV)
Voltage across a 75 ohm dipole at Sagepoint: 4.56 uV (13.18 dBuV)
Mode of propagation: Line of Sight
ITWOM error number: 0 (No error)

---------------------------------------------------------------------------


No obstructions to LOS path due to terrain were detected by SPLAT! HD

Antenna at Sagepoint must be raised to at least 17.00 feet AGL
to clear the first Fresnel zone.

Antenna at Sagepoint must be raised to at least 11.00 feet AGL
to clear 60% of the first Fresnel zone.
