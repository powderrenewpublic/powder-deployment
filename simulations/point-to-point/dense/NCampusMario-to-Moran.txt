
		--==[ SPLAT! HD v1.4.0 Path Analysis ]==--

---------------------------------------------------------------------------

Transmitter site: NCampusMario
Site location: 40.7729 North / 111.8409 West (40� 46' 22" N / 111� 50' 27" W)
Ground elevation: 4924.54 feet AMSL
Antenna height: 25.00 feet AGL / 4949.54 feet AMSL
Distance to Moran: 0.23 miles
Azimuth to Moran: 151.13 degrees
Depression angle to Moran: -0.3053 degrees

---------------------------------------------------------------------------

Receiver site: Moran
Site location: 40.7699 North / 111.8387 West (40� 46' 11" N / 111� 50' 19" W)
Ground elevation: 4917.98 feet AMSL
Antenna height: 25.00 feet AGL / 4942.98 feet AMSL
Distance to NCampusMario: 0.23 miles
Azimuth to NCampusMario: 331.13 degrees
Elevation angle to NCampusMario: +0.3019 degrees

---------------------------------------------------------------------------

ITWOM Version 3.0 Parameters Used In This Analysis:

Earth's Dielectric Constant: 5.000
Earth's Conductivity: 0.001 Siemens/meter
Atmospheric Bending Constant (N-units): 301.000 ppm
Frequency: 2100.000 MHz
Radio Climate: 5 (Continental Temperate)
Polarization: 0 (Horizontal)
Fraction of Situations: 50.0%
Fraction of Time: 50.0%
Transmitter ERP: 10.0 milliwatts (+10.00 dBm)
Transmitter EIRP: 16.4 milliwatts (+12.14 dBm)

---------------------------------------------------------------------------

Summary For The Link Between NCampusMario and Moran:

Free space path loss: 90.45 dB
ITWOM Version 3.0 path loss: 111.58 dB
Attenuation due to terrain shielding: 21.13 dB
Field strength at Moran: 44.26 dBuV/meter
Signal power level at Moran: -99.44 dBm
Signal power density at Moran: -101.52 dBW per square meter
Voltage across a 50 ohm dipole at Moran: 3.05 uV (9.69 dBuV)
Voltage across a 75 ohm dipole at Moran: 3.74 uV (11.45 dBuV)
Mode of propagation: Line of Sight
ITWOM error number: 4
  Warning: Some parameters are out of range.
  Results are probably invalid.

---------------------------------------------------------------------------


No obstructions to LOS path due to terrain were detected by SPLAT! HD

Antenna at Moran must be raised to at least 32.00 feet AGL
to clear the first Fresnel zone.

60% of the first Fresnel zone is clear.
