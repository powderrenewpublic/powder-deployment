
		--==[ SPLAT! HD v1.4.0 Path Analysis ]==--

---------------------------------------------------------------------------

Transmitter site: NCampusWasatch
Site location: 40.7711 North / 111.8432 West (40� 46' 15" N / 111� 50' 35" W)
Ground elevation: 4845.80 feet AMSL
Antenna height: 25.00 feet AGL / 4870.80 feet AMSL
Distance to GH: 0.45 miles
Azimuth to GH: 123.74 degrees
Elevation angle to GH: +1.2750 degrees

---------------------------------------------------------------------------

Receiver site: GH
Site location: 40.7675 North / 111.8361 West (40� 46' 2" N / 111� 50' 9" W)
Ground elevation: 4898.29 feet AMSL
Antenna height: 25.00 feet AGL / 4923.29 feet AMSL
Distance to NCampusWasatch: 0.45 miles
Azimuth to NCampusWasatch: 303.75 degrees
Depression angle to NCampusWasatch: -1.2814 degrees

---------------------------------------------------------------------------

ITWOM Version 3.0 Parameters Used In This Analysis:

Earth's Dielectric Constant: 5.000
Earth's Conductivity: 0.001 Siemens/meter
Atmospheric Bending Constant (N-units): 301.000 ppm
Frequency: 2100.000 MHz
Radio Climate: 5 (Continental Temperate)
Polarization: 0 (Horizontal)
Fraction of Situations: 50.0%
Fraction of Time: 50.0%
Transmitter ERP: 10.0 milliwatts (+10.00 dBm)
Transmitter EIRP: 16.4 milliwatts (+12.14 dBm)

---------------------------------------------------------------------------

Summary For The Link Between NCampusWasatch and GH:

Free space path loss: 96.03 dB
ITWOM Version 3.0 path loss: 117.52 dB
Attenuation due to terrain shielding: 21.49 dB
Field strength at GH: 38.33 dBuV/meter
Signal power level at GH: -105.38 dBm
Signal power density at GH: -107.46 dBW per square meter
Voltage across a 50 ohm dipole at GH: 1.54 uV (3.75 dBuV)
Voltage across a 75 ohm dipole at GH: 1.89 uV (5.51 dBuV)
Mode of propagation: Line of Sight
ITWOM error number: 4
  Warning: Some parameters are out of range.
  Results are probably invalid.

---------------------------------------------------------------------------


No obstructions to LOS path due to terrain were detected by SPLAT! HD

The first Fresnel zone is clear.

60% of the first Fresnel zone is clear.
