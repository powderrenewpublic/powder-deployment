
		--==[ SPLAT! HD v1.4.0 Path Analysis ]==--

---------------------------------------------------------------------------

Transmitter site: NCampusWasatch
Site location: 40.7711 North / 111.8432 West (40� 46' 15" N / 111� 50' 35" W)
Ground elevation: 4845.80 feet AMSL
Antenna height: 25.00 feet AGL / 4870.80 feet AMSL
Distance to NCampusMario: 0.17 miles
Azimuth to NCampusMario: 44.19 degrees
Elevation angle to NCampusMario: +4.9887 degrees

---------------------------------------------------------------------------

Receiver site: NCampusMario
Site location: 40.7729 North / 111.8409 West (40� 46' 22" N / 111� 50' 27" W)
Ground elevation: 4924.54 feet AMSL
Antenna height: 25.00 feet AGL / 4949.54 feet AMSL
Distance to NCampusWasatch: 0.17 miles
Azimuth to NCampusWasatch: 224.20 degrees
Depression angle to NCampusWasatch: -4.9912 degrees

---------------------------------------------------------------------------

ITWOM Version 3.0 Parameters Used In This Analysis:

Earth's Dielectric Constant: 5.000
Earth's Conductivity: 0.001 Siemens/meter
Atmospheric Bending Constant (N-units): 301.000 ppm
Frequency: 2100.000 MHz
Radio Climate: 5 (Continental Temperate)
Polarization: 0 (Horizontal)
Fraction of Situations: 50.0%
Fraction of Time: 50.0%
Transmitter ERP: 10.0 milliwatts (+10.00 dBm)
Transmitter EIRP: 16.4 milliwatts (+12.14 dBm)

---------------------------------------------------------------------------

Summary For The Link Between NCampusWasatch and NCampusMario:

Free space path loss: 87.73 dB
ITWOM Version 3.0 path loss: 108.47 dB
Attenuation due to terrain shielding: 20.75 dB
Field strength at NCampusMario: 47.37 dBuV/meter
Signal power level at NCampusMario: -96.33 dBm
Signal power density at NCampusMario: -98.41 dBW per square meter
Voltage across a 50 ohm dipole at NCampusMario: 4.36 uV (12.80 dBuV)
Voltage across a 75 ohm dipole at NCampusMario: 5.34 uV (14.56 dBuV)
Mode of propagation: Line of Sight
ITWOM error number: 4
  Warning: Some parameters are out of range.
  Results are probably invalid.

---------------------------------------------------------------------------


No obstructions to LOS path due to terrain were detected by SPLAT! HD

The first Fresnel zone is clear.

60% of the first Fresnel zone is clear.
