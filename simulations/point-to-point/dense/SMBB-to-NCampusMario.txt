
		--==[ SPLAT! HD v1.4.0 Path Analysis ]==--

---------------------------------------------------------------------------

Transmitter site: SMBB
Site location: 40.7685 North / 111.8404 West (40� 46' 6" N / 111� 50' 25" W)
Ground elevation: 4852.36 feet AMSL
Antenna height: 25.00 feet AGL / 4877.36 feet AMSL
Distance to NCampusMario: 0.30 miles
Azimuth to NCampusMario: 355.66 degrees
Elevation angle to NCampusMario: +2.5994 degrees

---------------------------------------------------------------------------

Receiver site: NCampusMario
Site location: 40.7729 North / 111.8409 West (40� 46' 22" N / 111� 50' 27" W)
Ground elevation: 4924.54 feet AMSL
Antenna height: 25.00 feet AGL / 4949.54 feet AMSL
Distance to SMBB: 0.30 miles
Azimuth to SMBB: 175.66 degrees
Depression angle to SMBB: -2.6038 degrees

---------------------------------------------------------------------------

ITWOM Version 3.0 Parameters Used In This Analysis:

Earth's Dielectric Constant: 5.000
Earth's Conductivity: 0.001 Siemens/meter
Atmospheric Bending Constant (N-units): 301.000 ppm
Frequency: 2100.000 MHz
Radio Climate: 5 (Continental Temperate)
Polarization: 0 (Horizontal)
Fraction of Situations: 50.0%
Fraction of Time: 50.0%
Transmitter ERP: 10.0 milliwatts (+10.00 dBm)
Transmitter EIRP: 16.4 milliwatts (+12.14 dBm)

---------------------------------------------------------------------------

Summary For The Link Between SMBB and NCampusMario:

Free space path loss: 92.62 dB
ITWOM Version 3.0 path loss: 113.61 dB
Attenuation due to terrain shielding: 20.99 dB
Field strength at NCampusMario: 42.24 dBuV/meter
Signal power level at NCampusMario: -101.47 dBm
Signal power density at NCampusMario: -103.55 dBW per square meter
Voltage across a 50 ohm dipole at NCampusMario: 2.42 uV (7.66 dBuV)
Voltage across a 75 ohm dipole at NCampusMario: 2.96 uV (9.42 dBuV)
Mode of propagation: Line of Sight
ITWOM error number: 4
  Warning: Some parameters are out of range.
  Results are probably invalid.

---------------------------------------------------------------------------


No obstructions to LOS path due to terrain were detected by SPLAT! HD

Antenna at NCampusMario must be raised to at least 38.00 feet AGL
to clear the first Fresnel zone.

Antenna at NCampusMario must be raised to at least 26.00 feet AGL
to clear 60% of the first Fresnel zone.
