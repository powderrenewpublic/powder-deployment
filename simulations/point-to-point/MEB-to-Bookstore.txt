
		--==[ SPLAT! HD v1.4.0 Path Analysis ]==--

---------------------------------------------------------------------------

Transmitter site: MEB
Site location: 40.7688 North / 111.8458 West (40� 46' 7" N / 111� 50' 44" W)
Ground elevation: 4793.31 feet AMSL
Antenna height: 50.00 feet AGL / 4843.31 feet AMSL
Distance to Bookstore: 0.33 miles
Azimuth to Bookstore: 196.03 degrees
Depression angle to Bookstore: -3.9180 degrees

---------------------------------------------------------------------------

Receiver site: Bookstore
Site location: 40.7641 North / 111.8476 West (40� 45' 50" N / 111� 50' 51" W)
Ground elevation: 4717.85 feet AMSL
Antenna height: 5.00 feet AGL / 4722.85 feet AMSL
Distance to MEB: 0.33 miles
Azimuth to MEB: 16.03 degrees
Elevation angle to MEB: +3.9132 degrees

---------------------------------------------------------------------------

ITWOM Version 3.0 Parameters Used In This Analysis:

Earth's Dielectric Constant: 5.000
Earth's Conductivity: 0.001 Siemens/meter
Atmospheric Bending Constant (N-units): 301.000 ppm
Frequency: 2500.000 MHz
Radio Climate: 5 (Continental Temperate)
Polarization: 0 (Horizontal)
Fraction of Situations: 50.0%
Fraction of Time: 50.0%
Transmitter ERP: 10.0 milliwatts (+10.00 dBm)
Transmitter EIRP: 16.4 milliwatts (+12.14 dBm)

---------------------------------------------------------------------------

Summary For The Link Between MEB and Bookstore:

Free space path loss: 95.04 dB
ITWOM Version 3.0 path loss: 116.07 dB
Attenuation due to terrain shielding: 21.03 dB
Field strength at Bookstore: 41.29 dBuV/meter
Signal power level at Bookstore: -103.93 dBm
Signal power density at Bookstore: -104.50 dBW per square meter
Voltage across a 50 ohm dipole at Bookstore: 1.82 uV (5.20 dBuV)
Voltage across a 75 ohm dipole at Bookstore: 2.23 uV (6.96 dBuV)
Mode of propagation: Line of Sight
ITWOM error number: 4
  Warning: Some parameters are out of range.
  Results are probably invalid.

---------------------------------------------------------------------------


No obstructions to LOS path due to terrain were detected by SPLAT! HD

The first Fresnel zone is clear.

60% of the first Fresnel zone is clear.
