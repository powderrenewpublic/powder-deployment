# POWDER deployment

`powder-deployment.csv` is a spreadsheet recording the locations of all
POWDER stations.  The fixed endpoint and rooftop stations are currently
operational, and the dense area stations are scheduled for the upcoming
phase of deployment.  `configuration.csv` provides specifications for
the equipment.

The `hardware` directory contains specifications for components installed
in POWDER.  `simulations` records output from CloudRF calculations.

The geographic scope of the project is documented in the
`areas-of-interest` directory.
